import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";

export default class Parent extends React.Component {
  constructor(props) {
    super(props);

    // Bind the this context to the handler function
    this.handler = this.handler.bind(this);

    // Set some state
    this.state = {
      messageShown: false
    };
  }

  // This method will be sent to the child component
  handler(param1) {
    console.log(param1);
    this.setState({
      messageShown: true
    });
  }

  // Render the child component and set the action property with the handler as value
  render() {
    return <Child action={() => this.handler("Parameter")} />;
  }
}

export class Child extends React.Component {
  render() {
    return (
      <View>
        {/* The button will execute the handler function set by the parent component */}
        <Button onClick={this.props.action.bind(this, param1)}>Test</Button>
      </View>
    );
  }
}
