import React, { Component } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

export class PropButton extends Component {
  render() {
    return (
      <TouchableOpacity style={this.propButtonStyles.button} onClick={this.props.handler}>
        <View style={this.propButtonStyles.circle}></View>
        <Text style={this.propButtonStyles.buttonFont}>{this.props.color}</Text>
      </TouchableOpacity>
    );
  }

  propButtonStyles = StyleSheet.create({
    buttonFont: {
      color: "#000",
      fontSize: 30,
      alignSelf: "center"
    },
    circle: {
      width: 20,
      height: 20,
      borderRadius: 100 / 2,
      borderWidth: 2,
      borderColor: "#ffffff",
      backgroundColor: this.props.color,
      margin: 20
    },
    button: {
      width: "90%",
      flexDirection: "row",
      borderWidth: 2,
      borderColor: "#ffffff",
      borderRadius: 5,
      paddingTop: 5,
      paddingBottom: 5,
      backgroundColor: "rgba(255,255,255,0.4)",
      margin: 10
    }
  });
}

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { color: "red" };
    this.handler = this.handler.bind(this);
  }

  handler(color) {
    console.log(color);
    this.setState({
      color: color
    });
  }
  render() {
    var colors = ["red", "yellow", "blue", "green", "rgb(255,120,1)", "salmon"];
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          backgroundColor: this.state.color,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        {colors.map((name, index) => {
          return <PropButton key={index} color={name} handler={() => this.handler(name)} />;
        })}
      </View>
    );
  }
}
