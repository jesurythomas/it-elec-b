// This covers Syntax

// How to declare variables
var x,y,z;
// How to assign values
x = 10; y = 5;
// How to compute values 
z = x + y;


// Literals 
99.99
100

// Strings can either be written within double or single quotes
"Daniel"
'Daniel'

// Operators (arithmetic)
(x + y) / z

// Operators (assignment)
z = 5;


//Expressions
5 * 5
z * 5
"Daniel" + " " + "Daniel"


// This is a comment 
// A comment is green

//JS is case sensitive 
var firstName; // An example of camel case
var firstname;
firstname = "Noel";
firstName = "Daniel;"