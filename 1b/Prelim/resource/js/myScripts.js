var searchTerms = [
    "chia seed",
    "gaslamp",
    "size terno",
    "alhambra",
    "san mig dark",
    "adhesive deo",
    "under odor",
    "air bed",
]

var localStorageVariable = [];
var itemIndex = [];

function load() {
    for (var content of searchTerms) {
        document.getElementById("popularSearches").innerHTML += "<div>" + content + "</div>";
    }
    init();
}

function loadJSON(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', './resource/JSON/saleObjects.JSON', true); // Replace 'my_data' with the path to your file
    xobj.onreadystatechange = function() {
        if (xobj.readyState == 4 && xobj.status == "200") {
            // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
            callback(xobj.responseText);
        }
    };
    xobj.send(null);
}

function init() {
    loadJSON(function(response) {
        // Parse JSON string into object
        var actual_JSON = JSON.parse(response);
        for (x of actual_JSON.saleObjects) {
            document.getElementById("lower").innerHTML += "<div class=labeledImage><img src=\"" + x.imageUrl + "\"><div>" + x.categoryName + "</div></div>";
        }
    });
}

function loginButton() {
    var modal = document.getElementById('id01');
    var body = document.getElementById('background');
    modal.style.display = 'block';
    body.style.overflow = "hidden";
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
            body.style.overflow = "auto";
        }
    }
}

function addToStorage(url, comment) {
    if (url == "") {
        payload = "<div class=labeledImage><img src=\"" + "https://via.placeholder.com/110x160?text=404" + "\"><div>" + comment + "</div></div>"
    } else {
        payload = "<div class=labeledImage><img src=\"" + url + "\"><div>" + comment + "</div></div>"
    }
    localStorageVariable.push(payload)
    itemIndex.push(comment)
    updateContent("crudContent");
}

function updateContent(target) {
    page = document.getElementById(target)
    page.innerHTML = "<div id=\"AdminPanel\">" + document.getElementById("AdminPanel").innerHTML + "</div>";
    for (x in localStorageVariable) {
        page.innerHTML += localStorageVariable[x];
    }
    document.getElementById("entrySelector").innerHTML = "";
    for (x in itemIndex) {
        updateIndexDropDown(itemIndex[x], x);
    }
}

function addButton() {
    addToStorage(document.getElementById("imgURL").value, document.getElementById("comment").value)
}

function updateIndexDropDown(comment, index) {
    payload = "<option value=\"" + index + "\">" + index + " - " + comment + "</option>"
    document.getElementById("entrySelector").innerHTML += payload
}

function deleteButton() {
    index = document.getElementById("entrySelector").value;
    itemIndex.splice(index, 1);
    localStorageVariable.splice(index, 1);
    updateContent("crudContent");
}