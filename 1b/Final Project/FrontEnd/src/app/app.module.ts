import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './display/navigation/navigation.component';
import { RegistrationComponent } from './display/registration/registration.component';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './display/login/login.component';
import { GetJournalsComponent } from './display/get-journals/get-journals.component';
import { NewJournalComponent } from './create/new-journal/new-journal.component';
import { EditPostComponent } from './create/edit-post/edit-post.component';
@NgModule({
  declarations: [AppComponent, NavigationComponent, RegistrationComponent, LoginComponent, GetJournalsComponent, NewJournalComponent, EditPostComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
