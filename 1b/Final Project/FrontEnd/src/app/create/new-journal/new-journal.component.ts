import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { environment } from './../../../environments/environment.prod';
import { Router } from '@angular/router';
@Component({
  selector: 'app-new-journal',
  templateUrl: './new-journal.component.html',
  styleUrls: ['./new-journal.component.css']
})
export class NewJournalComponent implements OnInit {
  getToken;
  constructor(private router: Router) {}

  ngOnInit() {
    this.getToken = sessionStorage.getItem('auth-token');
  }

  postContent(reqTitle, reqMessage, reqImgUrl) {
    const payload = {
      title: reqTitle,
      message: reqMessage,
      imageUrl: reqImgUrl
    };

    console.log(payload);
    axios
      .post(environment.API + '/journal', payload, { headers: { 'auth-token': this.getToken } })
      .then((response) => {
        console.log(response.status);
        this.router.navigate(['/get-journals'], { skipLocationChange: true });
      })
      .catch((error) => {
        console.log(error.response.status);
        alert(error.response.data);
      });
  }
  logOut() {
    sessionStorage.clear();
    this.router.navigate(['/login'], { skipLocationChange: true });
  }

  back() {
    this.router.navigate(['/get-journals'], { skipLocationChange: true });
  }
}
