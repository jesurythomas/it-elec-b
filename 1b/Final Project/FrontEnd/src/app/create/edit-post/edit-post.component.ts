import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { environment } from './../../../environments/environment.prod';
import { Router } from '@angular/router';
@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {
  getToken = sessionStorage.getItem('auth-token');
  currentPost = {
    title: '',
    message: '',
    imageUrl: ''
  };
  postID = sessionStorage.getItem('postID');
  constructor(private router: Router) {}

  ngOnInit() {
    axios
      .get(environment.API + '/journal/' + this.postID, {
        headers: { 'auth-token': this.getToken }
      })
      .then((response) => {
        this.currentPost = response.data;
      })
      .catch((error) => {
        console.log(error.response);
        alert(error.response.status + error.response.data);
      });
  }

  logOut() {
    sessionStorage.clear();
    this.router.navigate(['/login'], { skipLocationChange: true });
  }

  back() {
    this.router.navigate(['/get-journals'], { skipLocationChange: true });
  }

  editPost(reqTitle, reqMessage, reqImgUrl) {
    const payload = {
      title: reqTitle,
      message: reqMessage,
      imageUrl: reqImgUrl
    };
    console.log(payload);
    axios
      .put(environment.API + '/journal/' + this.postID, payload, {
        headers: { 'auth-token': this.getToken }
      })
      .then((response) => {
        console.log(response.status);
        this.router.navigate(['/get-journals'], { skipLocationChange: true });
      })
      .catch((error) => {
        console.log(error.response.status);
        alert(error.response.data);
      });
  }
}
