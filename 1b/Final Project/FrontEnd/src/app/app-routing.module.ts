import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './display/registration/registration.component';
import { GetJournalsComponent } from './display/get-journals/get-journals.component';
import { LoginComponent } from './display/login/login.component';
import { NewJournalComponent } from './create/new-journal/new-journal.component';
import { EditPostComponent } from './create/edit-post/edit-post.component';
const routes: Routes = [
  { path: 'registration', component: RegistrationComponent },
  { path: 'login', component: LoginComponent },
  { path: 'get-journals', component: GetJournalsComponent },
  { path: 'new-journal', component: NewJournalComponent },
  { path: 'edit-post', component: EditPostComponent },
  { path: '', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
