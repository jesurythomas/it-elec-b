import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { environment } from './../../../environments/environment.prod';
import { Router } from '@angular/router';
@Component({
  selector: 'app-get-journals',
  templateUrl: './get-journals.component.html',
  styleUrls: ['./get-journals.component.css']
})
export class GetJournalsComponent implements OnInit {
  getToken = sessionStorage.getItem('auth-token');
  journals;

  constructor(private router: Router) {}

  getJournals() {
    axios
      .get(environment.API + '/journals', { headers: { 'auth-token': this.getToken } })
      .then((response) => {
        this.journals = response.data;
        for (const i of this.journals) {
          i.updatedAt = new Date(i.updatedAt).toUTCString();
          i.createdAt = new Date(i.createdAt).toUTCString();
        }
      })
      .catch((error) => {
        console.log(error.response);
        if (error.response.status === 403) {
          alert('You need to be logged in to access this page. Please login');
          this.router.navigate(['/login'], { skipLocationChange: true });
        }
      });
  }

  showDates(timeValue_1, timeValue_2) {
    return timeValue_1 > timeValue_2;
  }

  edit(id) {
    sessionStorage.setItem('postID', id);
    this.router.navigate(['/edit-post'], { skipLocationChange: true });
    console.log(id);
  }

  delete(id) {
    axios
      .delete(environment.API + '/journal/' + id, {
        headers: { 'auth-token': this.getToken }
      })
      .then((response) => {
        location.reload();
      });
  }

  add() {
    this.router.navigate(['/new-journal'], { skipLocationChange: true });
  }

  logOut() {
    sessionStorage.clear();
    this.router.navigate(['/login'], { skipLocationChange: true });
  }
  ngOnInit() {
    this.getJournals();
  }
}
