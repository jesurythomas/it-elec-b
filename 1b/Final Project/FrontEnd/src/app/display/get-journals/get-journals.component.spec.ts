import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetJournalsComponent } from './get-journals.component';

describe('GetJournalsComponent', () => {
  let component: GetJournalsComponent;
  let fixture: ComponentFixture<GetJournalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetJournalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetJournalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
