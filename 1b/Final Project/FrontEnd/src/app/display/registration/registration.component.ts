import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import bcryptjs from 'bcryptjs';
import { environment } from './../../../environments/environment.prod';
import { Router } from '@angular/router';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  constructor(private router: Router) {}

  onSubmit(passEmail, passPass, passUser) {
    const salt = bcryptjs.genSaltSync(10);
    const hashedPassword = bcryptjs.hashSync(passPass, salt);

    axios
      .post(environment.API + '/user/create', {
        username: passUser,
        email: passEmail,
        password: hashedPassword
      })
      .then((response) => {
        console.log(response);
        alert('Registered Successfully');
        this.router.navigate(['/login'], { skipLocationChange: true });
      })
      .catch((error) => {
        console.log(error.response);
        alert(error.response.status + error.response.data);
      });
  }

  login() {
    this.router.navigate(['/login'], { skipLocationChange: true });
  }

  ngOnInit() {}
}
