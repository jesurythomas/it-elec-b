import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { environment } from './../../../environments/environment.prod';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  newToken;
  constructor(private router: Router) {}

  onSubmit(passUser, passPass) {
    console.log('sending request to',environment.API);
    axios
      .post(environment.API + '/user/login', {
        username: passUser,
        password: passPass
      })
      .then((response) => {
        console.log(response);
        this.newToken = response.data;
        sessionStorage.setItem('auth-token', this.newToken);
        this.router.navigate(['/get-journals'], { skipLocationChange: true });
      })
      .catch((error) => {
        console.log(error.response);
        document.getElementById('error').style.display = 'block';
        document.getElementById('error').innerHTML = error.response.data;
        setTimeout(() => {
          document.getElementById('error').innerHTML = '<br>';
        }, 3000);
      });
  }

  register() {
    this.router.navigate(['/registration'], { skipLocationChange: true });
  }
  ngOnInit() {
    if (sessionStorage.getItem('auth-token')) {
      this.router.navigate(['/get-journals'], { skipLocationChange: true });
    }
  }
}
