const mongoose = require("mongoose");

const journalSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true
    },
    message: {
      type: String,
      required: true
    },
    imageUrl: {
      type: String
    },
    ownerID: {
      type: String
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Journals", journalSchema);
