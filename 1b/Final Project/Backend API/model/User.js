const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    banStatus: {
      type: Boolean,
      default: false
    },
    following: {
      type: Array
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", userSchema);
