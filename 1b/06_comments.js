// You can write a comment like this

/* YOU CAN WRITE IT LIKE THIS
EVEN LIKE THIS
OR EVEN LIKE THIS */

// You can prevent executions with comments

//document.write("WOOOOOOOOO");
document.write("Only this is executed");

/*
document.write("WOOOOOOOOO");
document.write("Only this is executed");
*/

