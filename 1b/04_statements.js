//Statements in Javascript 
var a, b, c;        // Statement 1 
a = 10;             // Statement 2 
b = 20;             // Statement 3 
c = a + b;          // Statement 4


//Semicolons seperate statements
var x,y,z,v; x = 1; y = 2; z = 3; v = x + y + z;


// Whitespace is ignored
var name =       "Daniel"; 
// is the same as
var name = "Daniel";


