//Variable examples
//Containers for values
var x = 10;
var y = 15;
var z = x + y;

//We can hold values and use them in expressions
var thisVar_1 = 5;
var thisVar_2 = 6;
var total_var = thisVar_1 + thisVar_2;

// = is an assignment operator and not an equal to operator
x = x + 5;
// this makes the new X equal to 15

// Equal to is written with ==