/*jshint esversion: 6 */
const express = require("express");
const app = express();

app.get('/', (req,res) => res.send('Hello there! -General Kenobi'));

app.listen(3000, () => {
    console.log("API listening on port 3000!")
});

